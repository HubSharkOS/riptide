# Riptide

A set of bash scripts to build a Debian GNU/Linux based Operating System,  
along with our branding. The HubShark GNU/Linux distribution is built using these 
scripts, so this may be biased towards the project. 


## Documentation

See the [wiki](../../wikis/home) for more details.


## How to use


#### Step 1
  * ./core-brand
    - The core branding on a fresh install

#### Step 2
  * ./nox
    - installs software usefull on all systems
    - does not require an X-WINDOW

~~#### Step 3~~
  ~~* ./desktop~~
    ~~- Get the packages we want on our base Desktop~~

#### Step 4
  * ./DE-ext
    - Extends the packages (used for all DEs) 

#### Step 5
  * ./codecs
    - get various codecs (Debian "non-free")
  
#### Step 6
  * ./get-webkit
  - needed for our live installer
  - not in squeeze yet, so we got debs from _sid_


### Release Types

Releases are done in the form of 
_DISTRIBUTION SHORT NAME-DIST VERSION.WhatToBuild.$(uname -m).CODENAME-date_


So we would have something like:
hubshark-9.0preview.wac.x86_64.bruce-20170125-2348.iso

*WhatToBuild* plays an important rule when downloading your iso. Here's the list:

* wac = With All Codecs
  - some of these may not be allowed in your country
  - if you are not sure, download the _noc_
* noc = No Codecs
  - we have no codecs installed, this is left up to you
* wsc = With Some Codecs
  - we don't add codecs that may cause problems in certain coutntries
  - it's left up to you to check if this is ok for you



## License(s) Used

Remastersys & Snapper are released under the GPL.

The rest, unless explicitly noted, are under the MIT License.

This pertains to the scripts that make up this package.
